# July 2020

- Set up initial repositories
- Decided on Gitlab as primary repo
- Configured Mirrors to Github
- Generated initial logos on Hatchful (Shopfiy logo)
- Started on Home & Jobs Repos
- Added some coming soon tweets